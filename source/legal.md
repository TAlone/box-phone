---
title: Mentions légales
author: Stalone
---

# Introduction

## Présentation

« Box Phone », le blog de [@talone@cybre.space](https://cybre.space/@talone) est un service en ligne de communication de l’information au public soumis au régime juridique français et édité à titre non-professionnel ; conformément aux dispositions de l’article 6, III, 2 de la loi n°2004-575 du 21 juin 2004 pour la confiance dans l’économie numérique, ci-après nommée « LCEN » ou encore « loi CEN », son éditeur a choisi de rester anonyme. Ce site est un site personnel tenu par un particulier ; ni sa famille, ni ses proches, ni son école ou université, ni son employeur ne peuvent être tenus comme responsables du contenu publié sur ce site, ni n’ont un droit de regard sur celui-ci.

<+++>

## Réclamation

En cas de réclamation sur le contenu de ce site, vous pouvez contacter l’éditeur par courriel à l’adresse suivante : `reclamation [arobase] boxph [point] one` ; dans le cas où celui-ci ne donnerais pas suite à vos sollicitations, la loi vous permet de vous adresser directement à l’hébergeur de ce site, dont les coordonnées sont :

> Hetzner Online GmbH
>
> Industriestr. 25
>
> 91710 Gunzenhausen
>
> Germany
>
> Tel.: +49 (0)9831 505-0
>
> Fax: +49 (0)9831 505-3
>
> E-Mail: `info [arobase] hetzner [point] com`

Les coordonnées de l’éditeur du site sont connues de l’hébergeur ; toutefois, afin d’éviter tout abus, l’article 6, I, 4 de la loi n°2004-575, dite LCEN dispose « le fait, pour toute personne, de présenter aux hébergeurs un contenu ou une activité comme étant illicite dans le but d’en obtenir le retrait ou d’en faire cesser la diffusion, alors qu’elle sait cette information inexacte, est puni d’une peine d’un an d’emprisonnement et de 15 000 Euros d’amende ».

# Responsabilité et propriété intellectuelle

## Responsabilité de l’auteur

L’auteur de ce site prends la responsabilité de ses écrits ; il est le seul à disposer d’un droit de regard sur l’ensemble du site et s’engage à supprimer toute publication ne respectant pas les présentes mentions légales.

## Engagement éditorial & rédactionnel

L’auteur de ce site est ouvert à toute discussion sur leurs publications et s’engage, par soucis de vérité, à corriger toute information erronée ; il s’engage à respecter le droit à l’image et la vie privée des personnes ainsi qu’à ne pas porter atteinte à la protection des personnes ou au maintient de l’ordre public ; il s’engage en particulier à respecter la loi française, notamment la loi sur la liberté de la presse du 29 juillet 1881 et la loi du 21 juin 2004 pour la confiance dans l’économie numérique sus-citée.

## Limitation de responsabilité

Les informations présentes sur le site sont fournies en l’état, ainsi, l’éditeur du site ne peut garantir l’exactitude ou la pertinence de ces données ; en outre, les informations mises à disposition sur ce site le sont uniquement à titre purement pédagogique et informatif, l’utilisateur du site assume donc l’ensemble des risques découlant de l’utilisation des informations présentes sur le site ou dans les sites joignables au travers des liens hypertextes. 

## Propriété intellectuelle

Sauf mention contraire, tous les articles de ce site sont mis à disposition sous licence [CC-BY-SA](https://creativecommons.org/licenses/by-sa/2.0/legalcode.fr), dont le texte intégral peut être trouvé en cliquant sur le lien ci-avant.

Tous les codes sources originaux sont soumis à la licence [Affero General Public Licence v3.0](https://www.gnu.org/licenses/agpl-3.0.en.html), pouvant être trouvée au lien précédant ou via le [dépôt Git du blog](https://framagit.org/TAlone/box-phone), dont l’entièreté du code est soumise à cette même licence.

L’auteur de ce site ne saurait être tenu responsable ni des liens sortants présents sur ce site, ni des contenus présentés sur ces liens, ni ne peut garantir la licence de ces contenus ; l’éditeur du site dédie les présentes mentions légales au domaine public.

La police utilisée sur ce site est « Playfair Display » de Claus Eggers Sørensen, publiée sous licence SIL Open Font License v1.10 ; l’icône de ce blog est « Phone Box » par Scott Lewis du Noun Project, distribuée sous licence CC-BY.

# Données personnelles

Aucune donnée à caractère personnelle personnelle n’est collectée lors de votre navigation sur le site ; aucun tracker ni aucune publicité n’y est présente, et les données sont toutes chargées localement. Les adresses IP des visiteurs peuvent être stockées dans les fichiers de journalisation du serveur ; ces données, ne pouvant être recoupés avec aucune autre, ne constituent pas des données personnelles au sens du Règlement Général pour la Protection des Données, et ne sont conservés que 7 jours ; elles ne sont utilisés qu’à des fins de diagnostic des problèmes par l’administrateur système.

# Droit applicable

L’éditeur s’efforce de garantir que l’accès au site soit possible depuis n’importe où et n’importe quand sauf en cas de force majeure ou d’un événement hors de contrôle de l’éditeur et sous réserve des éventuelles pannes et interventions de maintenance nécessaires au bon fonctionnement du site, de ce fait, la responsabilité de l’éditeur ne peut être engagée en cas d’impossibilité d’accès à ce site ou d’utilisation des services proposés par celui-ci ; il n’est prévu aucune assistance technique vis-à-vis des utilisateurs, toutefois, si vous constatez une panne, il est possible d’en informer l’éditeur par courriel, celui-ci remettra le site en place dans les plus brefs délais.

La durée des présentes mentions légales est indéterminée et l’éditeur se réserve le droit de les modifier, à tout moment et sans préavis ; la législation française s’applique aux présentes mentions ; en cas de litige, n’ayant pu faire l’objet d’un accord à l’amiable, seuls les tribunaux français du ressort de la cour d’appel de Paris sont compétents.
