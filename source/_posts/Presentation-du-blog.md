---
title: Présentation du blog
author: Stalone
date: 2019-08-01 00:00:00
categories:
- [droit]
- [internet]
- [electronique]
---

Bienvenue à tous ! Après des années à trainer sur l’ARPANET digital, j’ouvre finalement un blog pour partager plein de choses qui me tiennent à cœur (ou non). Ce blog est fondé par la volonté de faire tenir des pensées que je ne peux manifestement pas publier sur Mastodon en raison de la limite de caractères. Vous trouverez par ici principalement de l’informatique saupoudré d’une touche d’électronique (pas seulement numérique) à propos de mes réalisations et de mes espoirs.

<+++>

# Présentations

Vous êtes encore là après cette introduction ? Chouette ! Alors commençons par nous présenter : je suis connu sur Internet comme TAlone, avec quelques variantes selon les sites. Le meilleur moyen de me contacter est [Mastodon](https://cybre.space/@talone), mais si vous êtes timide, je suis aussi joignable par mail à [talone@boxph.one](mailto:talone@boxph.one). Le blog étant un moyen très vertical de partager l’information, le débat ne peut s’ouvrir qu’ailleurs, n’hésitez donc pas à poser des questions via les moyens sus-mentionnés. Je suis par ailleurs très présent sur [Zeste de Savoir](https://zestedesavoir.com/membres/voir/TAlone/), qui est un site communautaire de partage de connaissance ; j’y ai rédigé quelques cours et articles et interviens parfois sur le forum.

## Qui suis-je ?

Ma légitimité à ouvrir un blog est égale à celle que vous voudrez bien me donner ; je n’ai pas la prétention de forcer quiconque à lire ce que j’écris, donc restez pour lire ou voguez vers d’autres horizons : les réseaux sont vastes et vous n’aurez jamais fini de les explorer.

Afin de vous donner envie de lire tout de même, laissez-moi vous raconter un peu d’où je viens : né en plein boom d’Internet, je m’intéresse très vite aux machines et à leur fonctionnement, en apprenant la programmation, d’abord en C, puis en d’autres langages qui me permettent aujourd’hui d’être autonome devant un code quelconque – quoique, peut-être pas en COBOL. J’apprends par la suite le développement web, et arrive à des résultats corrects malgré de piètres compétences en design – ce blog en est peut-être la preuve. Aujourd’hui, je suis surtout intéressé par le réseau et l’auto-hébergement, et par le développement de noyaux, en particulier pour faire de l’embarqué.

En parlant d’électronique, je me lance dans l’électronique quelques années après, avec l’informatique embarquée, à commencer par Arduino – comme beaucoup 😉, puis j’enchaine avec l’électronique analogique, qui me prend encore beaucoup de temps libre aujourd’hui, et je développe par la suite mes compétences en numérique par la programmation d’autres microcontrôleurs que les AVR (Arduino). À ce jour, je tente de programmer des processeurs un peu plus costauds comme les STM32 ou même, comme mentionné ci-dessus, de faire du développement noyau pour ordinateurs mono-cartes comme les Raspberry Pi, Orange Pi, Beaglebone, etc.

## Du droit ?

Les visions d’aujourd’hui des réseaux sont politiques ou ne sont pas ; il est absolument nécessaire de comprendre, d’analyser et, lorsque c’est possible, de réparer les mécanismes de régulation mis en place par des décisions qui ne nous conviennent pas. Ce travail complexe et pluridisciplinaire, en partie effectué par [La Quadrature du Net](https://www.laquadrature.net/), doit également être mené par les utilisateurs du réseau, autant que faire se peut. Il consiste en partie en des analyses juridiques, même grossières, qui sont en fait la transcription écrite de la volonté politique, et nous disent donc toujours plus que leurs articles.

C’est pourquoi je m’interesse également au droit, ou du moins pourquoi je m’y suis interessé au départ ; maintenant, ma vision est plus complète sur les engrenages de notre législation, et je pratique également – bien que modestement – le droit fiscal, dans lequel j’inclus les prélèvements (appelés impôts par le commun des mortels) et versements (aides sociales principalement). Je parlerais probablement souvent droit sur ce blog, que ce soit par des billets complets, ou comme partie d’un problème, parce que peu de sujet n’ont pas d’angle juridique.

# Contenu

Je vous propose une liste non-exhaustive de ce qui devrait arriver sur ce blog un moment ou un autre ; rien n’est garanti, et cette liste est amenée à changer et à être complétée au fil du temps ; considérez-la plutôt comme un guide que comme une ligne directive.

En termes d’informatique, je pense poster quelques billets sur l’autohébergement et en particulier la mise en place d’un serveur de mail ; je devrais aussi détailler mon projet de serveur SMTP expérimental. Autrement, une partie sur le réseau et notamment une transcription de ma conférence sur IPv6 est à prévoir ; quelques informations sur la compilation de *toolchains* devrait aussi arriver très vite, et d’autres articles non-attendus seront très probablement publiés.

Pour l’électronique, mon programme est beaucoup moins fixé, mais je prévois globalement de parler d’open-hardware, de réalisation de PCB en amateur et de radio, sûrement beaucoup de radio, d’ailleurs, puisque j’ai décidé de me lancer dans la pratique radioamateur, et de concevoir des équipements, au moins modestes ; mais avant ça, il faut passer la licence, donc attendez-vous à des articles là-dessus. Sur un tout autre sujet, j’aimerais traiter de l’*open-hardware* et de la manipulation de tensions élevées via des appareils conçus en amateur.

Enfin, parlons peu parlons droit, je proposerais un aperçu de ma demande du code source du simulateur d’Allocations Logement à la CAF, et je tenterais une série d’article expliquant le calcul des APL, de la prime d’activité et de l’impôt sur le revenu. Aussi, j’aimerais rédiger une sorte de tutoriel pour apprendre aux personnes interessées à faire divers recours. Comme vous pouvez le voir, le contenu sera très divers, et probablement aussi très dispersé dans le temps en fonction de mes disponibilités pour écrire.

# Technologie

Un petit mot à propos du fonctionnement de ce blog ; il est statique et généré par [Hexo](https://hexo.io/). Le thème est de ma conception, mais la police est « Playfair Display » de Claus Eggers Sørensen, publiée sous licence SIL Open Font License v1.10 ; l’icône de ce blog est quant à elle « Phone Box » par Scott Lewis du Noun Project, distribuée sous licence CC-BY. Les articles sont publiés sous licence [CC-BY-SA](https://creativecommons.org/licenses/by-sa/2.0/legalcode.fr) ; le code, et le thème aussi sont disponibles sur le [dépôt Git du blog](https://framagit.org/TAlone/box-phone) et placé sous licence [AGPL v3.0](https://www.gnu.org/licenses/agpl-3.0.en.html).

Niveau serveur, je suis sur un VPS [Hetzner](https://www.hetzner.com/) dont je suis très content ; le blog est servi par un reverse proxy nginx – dont je suis moins content, mais c’est une autre histoire ; le certificat est généré par la désormais célèbre entitée [Let’s Encrypt](https://letsencrypt.org/).

Merci d’avoir lu cette présentation et à bientôt pour le prochain article !
