---
title: Calcul de la prime d’activité
author: Stalone
date: 2019-08-16 00:00:00
categories:
- droit
---

Dans cet article, nous allons étudier le mode de calcul de la prime d’activité ; cette prime, prévue pour « inciter les travailleurs aux ressources modestes à exercer une activité » est un bon point de départ pour ceux qui souhaitent comprendre les mécanismes d’aides sociales, car son fonctionnement est relativement simple. En fonction du niveau voulu d’approfondissement, plusieurs manières plus ou moins avancées sont proposés pour la découverte de cette prime.

<+++>

Les différentes approches proposées ici sont les suivantes :

1. une explication de principe pour ceux souhaitant seulement comprendre ;
2. un détail des calculs et des références législatives pour approfondir ;
3. enfin, une partie code pour les plus téméraires, désirant expérimenter.

Si vous n’êtes pas intéressé par l’une de ces parties, il vous suffit de zapper ce que vous ne voulez pas voir. Les personnes souhaitant seulement comprendre comment la prime est calculée pour leur situation pourront lire l’article rapidement et surtout bien regarder les exemples, proposés pour faciliter la compréhension.

# Conditions d’obtention

La prime d’activité est soumise à des conditions d’ouverture plutôt larges, mais tend à décroitre facilement avec le salaire. Elle est versée **par travailleur**, mais dépend en grande partie des ressources du **foyer**[^1] ; pour être considéré comme *travailleur* au sens de la prime d’activité, les conditions suivantes doivent être remplies :

- la personne doit être âgée de 18 ans révolus[^2] ;
- elle doit travailler, et donc percevoir un **revenu** tiré de son activité professionnelle ;
- la personne doit être de nationalité française, ou disposer d’une exception spécifique prévue par la loi[^3] ;
- elle ne doit pas cumuler son emploi avec tout autre statut tiré de sa qualité d’étudiant[^4], sauf dans le cas où la personne est **détachée fiscalement**[^5] de ses parents ;
- de même, la prime d’activité n’est pas due aux travailleurs détachés – attention à la confusion : salarié détaché temporairement[^6] et personne détachée fiscalement sont deux termes bien différents.

Toutes ces conditions, et en particulier les exceptions prévues pour les travailleurs non-nationaux, peuvent être trouvées à l’article [L. 842-2 du Code de la Sécurité Sociale (CSS)](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000031087615&cidTexte=LEGITEXT000006073189&dateTexte=20190802)… De manière générale, les articles commençant par 84x dudit code régissent la prime d’activité, n’hésitez pas à les consulter pour plus de détails. Sans plus attendre, rentrons maintenant dans le calcul à proprement parler.

# Première étape : le montant forfaitaire

La prime d’activité ($P_{act}$) consiste en une différence, entre un montant forfaitaire $M_f$ calculé selon divers paramètres que nous verrons ci-dessous, et les revenus totaux du foyer[^7] notés $R_f$. On a donc :

$$
P_{act} = M_f - R_f
$$

Le montant forfaitaire est d’abord calculé selon une politique **familiale**, puis selon les revenus de chaque travailleur ; s’il y a plus d’une personne travaillant dans le foyer, il suffira d’additionner les contributions de chaque personne, pour obtenir à la fin le montant global dû au foyer. Si tout cela n’est pas très clair, n’hésitez pas à continuer tout de même, nous en reparlerons. Pour le moment, nous allons étudier la première partie de la prime : le montant forfaitaire, qui dépend d’un montant fixe, et d’un coefficient multiplicateur établi en fonction de la composition du foyer.

Tous les montants (salaires, prestations familiales, aides diverses, etc) sont des moyennes sur les trois derniers mois, puisque la prime d’activité est ainsi calculée. N’oubliez donc pas, si votre salaire ou vos aides sociales ne sont pas fixes sur l’année, de faire cette moyenne avant de calculer ce qui est proposé par la suite. Pour rappel, une moyenne de trois salaires $S_1$, $S_2$ et $S_3$ est effectuée comme suit :

$$
S = \frac{S_1 + S_2 + S_3}{3}
$$

## Les bases

Pour commencer, donnons la formule globale du montant forfaitaire : $M_f = M_{f_0} \times \mu$ avec $\mu$ le coefficient multiplicateur mentionné. Ce coefficient est égal à l’unité pour une personne seule et sans enfants, et est ensuite majoré de 50 % dans les cas où le foyer est composé d’au moins deux personnes (typiquement, lorsque, la personne est mariée ou PACSée)[^8]. Le coefficient est enfin augmenté en fonction du nombre d’enfants à charge.

Le montant $M_{f_0}$ est fixé tous (HERE) les ans par décret, autour du mois d’avril ; la dernière référence est le [décret n° 2018-836 du 3 octobre 2018 portant revalorisation du montant forfaitaire de la prime d’activité et réduction de l’abattement appliqué aux revenus professionnels](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000037460543), fixant son montant à 551,51 € pour l’année 2018/2019. *Exceptionnellement, cette revalorisation a été effectuée en octobre*.

## Les enfants

Les autres personnes à charge (suppléant les deux premières) ajoutent au taux $\mu$ différents coefficients, en fonction de leur nombre, et de s’il s’agit ou non d’enfants[^8] :

- 30 % pour toute personne à charge, sauf ;
- 40 % à partir de la troisième personne si ces trois personnes ont moins de 25 ans ; ainsi, les personnes rattachées au foyer mais qui ne sont plus des enfants n’ajoutent que le taux de 30 % sus-mentionné.

En général, retenez 30 % pour les deux premiers enfants, et 40 % ensuite, même s’il y a souvent des cas particuliers, qui nécessitent de prendre en compte longtemps le barème à 30 % au-delà de 2 personnes.

## Le barème « isolé »

Si la personne est isolée[^9], des taux particuliers sont applicables, et vont comme suit :

- le taux de base pour une personne isolée est de 128,412 %, contrairement à 100 % en temps normal ;
- puis, un coefficient de 42,804 % est appliqué **pour toute personne à charge**, qu’il s’agisse ou non d’un enfant.

Le tableau suivant résume les différentes valeurs de $\mu$ pour **les cas courants**, que la personne soit considérée isolée ou non :

| Nombre d’enfants | Personne seule | Couple |
| ---------------- | -------------- | ------ |
|        0         |     1          |  1,5   |
|        1         |     1,712      |  1,8   |
|        2         |     2,14       |  2,1   |
|        3         |     2,568      |  2,5   |
|        4         |     2,996      |  2,9   |
|        5         |     3,424      |  3,3   |
|        6         |     3,852      |  3,7   |

## Le fil conducteur : une famille avec trois enfants

Nous allons, pour tout l’article, illustrer la prime d’activité à partir d’un exemple simple et concret :

- une famille composée de deux personnes ;
- la première gagne 1 300 € net – tous nos calculs seront fait en net ;
- la seconde touche 1 100 € net par mois ;
- trois enfants sont rattachés au foyer, et n’ont aucun revenus.

D’après le tableau, on a $\mu = 2,5$, donc $M_f = 551,51 \times 2,5 = 1 379 €$, ce sera notre point de départ pour la suite.

## Un début de simulateur

Pour les plus informaticiens d’entre vous, je propose la partie du code que j’utilise pour faire les simulations de primes d’activité, réalisé en Clojure. On obtient alors le code suivant si l’on essaie de réaliser un simulateur complet du taux du foyer :

```clj
;; Fixés au même endroit que le mode de calcul des taux.
;; Référence de calcul : Article D843-1 du CSS
(def foyer_taux_base 0.7)
(def foyer_taux_personne 0.3)
(def foyer_taux_conjoint_maj 0.2)
(def foyer_taux_enfants_maj 0.1)

(def foyer_taux_isole 1.28412)
(def foyer_taux_isole_personne 0.42804)

;; Référence de calcul : Article D843-1 du CSS
(defn calcul_taux_foyer [personnes enfants isole]
	(if (not isole) (+
		foyer_taux_base
		(* foyer_taux_personne personnes)
		(if (> personnes 1) foyer_taux_conjoint_maj 0)
		(if (> enfants 2) (* foyer_taux_enfants_maj (- enfants 2)) 0)
	) (+
		foyer_taux_isole
		(* foyer_taux_isole_personne personnes)
	))
)
```

# La dépendance au salaire individuel et les bonifications

Pour chaque membre du foyer ayant un revenu professionnel, une bonification individuelle $B_i$ peut être versée en fonction du salaire ; elle est d’autant plus importante que le salaire est important. La bonification s’exprime **en pourcentage** du montant forfaitaire de base $M_{f_0}$ et en fonction du SMIC horaire brut[^10] ; on distingue trois cas possibles (tous les montants sont donnés pour le premier semestre 2019) :

- en dessous de 59 SMICs (soit un demi-SMIC mensuel), soit 591,77 € pour un mois, le montant de la bonification est nul ;
- au-dessus de 120 SMICs (soit un SMIC mensuel), soit 1 204 €, le montant est plafonné à 29,01 % de $M_{f_0}$, soit 160 € ;
- entre 59 SMICs et 120 SMICs, la variation est linéaire.

**Petite note d’actualité** : lorsque la revalorisation de la prime d’activité a été annoncée en décembre 2018, la revalorisation portait sur ces bonifications individuelles[^11]. Les barèmes précédents portait sur 95 SMICs au lieu de 120 et 12,782 % au maximum contre 29,101 % ; ainsi, une personne touchant 952 € par mois, soit l’ancien taux maximal, ramené à l’inflation sur 2019, bénéficie de 24 € d’augmentation de la prime d’activité.

## Variation linéaire

Les deux premiers cas sont simples, mais le dernier cas peut s’avérer délicat ; je vous propose donc une formule simple pour le calcul de ce dernier cas[^12] :

$$
B_i = \delta \times (S_{ind} - B_{i_0})
$$

Avec $S_{ind}$ le salaire de la personne concernée, en net, et $\delta$ et $B_{i_0}$ deux coefficients variants en fonction de la valeur du SMIC horaire brut chaque année ainsi que du montant de référence $M_{f_0}$. Pour référence, vous trouverez ci-dessous les calculs pour trouver ces variables, ainsi que leurs valeurs pour le premier semestre 2019 :

- $$\delta = \frac{ \tau \times M_{f_0} }{ 100 \times S_{MIC} \times (M - m) } = 0,2615$$ ;
- $$B_{i_0} = m \times S_{MIC} = 591,77 €$$ ; notez que ce montant est le montant à partir duquel la bonification individuelle atteint un montant non-nul.

## Multiples personnes

Lorsque plusieurs personnes dans le foyer disposent d’un revenu, la bonification doit être calculée **pour chaque personne**, puis chaque bonification est additionnée au montant forfaitaire majoré dépendant de la situation familiale, ce qui permet alors d’obtenir le montant forfaitaire dit « majoré », exprimé formellement comme ceci :

$$
M_f' = M_f + \sum^{pers.} B_i
$$

## Petite étape intermédiaire

Avant de passer à la partie suivante concernant le calcul des ressources de foyer, il nous reste une étape importante sur le montant forfaitaire majoré, que j’ai occultée jusqu’ici. Il faut rajouter au montant calculé une fraction du salaire individuel en même temps que l’ajout de la bonification[^13]. Cette fraction s’élève aujourd’hui à 61 % ; elle change parfois de quelques points selon les gouvernements[^14] ; on a donc la formule finale du *montant forfaitaire majoré* :

$$
M_f' = M_f + \sum^{pers.} { B_i + \frac{61}{100} \times S_i }
$$

Où $S_i$ est le **salaire** de la personne dont on a calculé la bonification précédemment ; *seuls les revenus professionnels* sont pris en compte ici, les aides diverses ne comptent pas pour la fraction du revenu individuel.

## Reprenons notre exemple

Cette sale formule sera certainement plus claire avec un exemple : nous avions tout à l’heure calculé le montant forfaitaire pour notre famille ; continuons en calculant leurs bonifications individuelles :

- la personne ayant 1 300 € atteint le plafond, et bénéficiera donc de 160 € de bonification ;
- la personne ayant 1 100 € par mois aura une bonification de $B_{i_2} = 0,2615 \times (1 100 - 591,77) = 132,902 €$.

En faisant l’addition, on trouve le montant forfaitaire **majoré** :

$$
\begin{matrix}
M_f'	&=& M_f + B_{i_1} + \frac{61}{100} \times S_{i_1} + B_{i_2} + \frac{61}{100} \times S_{i_2} \\
		&=& 1379 + 160 + \frac{61}{100} \times 1 300 + 132,902 + \frac{61}{100} \times 1 100 \\
		&=& 1379 + 160 + 793 + 132,902 + 671 \\
M_f'	&=& 3 136 €
\end{matrix}
$$

## Code complet du montant forfaitaire

Il est maintenant possible d’ajouter à notre simulateur le calcul de cette fraction des revenus, ainsi que les bonifications individuelles :

```clj
;; Le SMIC est revalorisé par rapport à l'inflation chaque année puis publié.
;; La valeur ici est entendue en brut. Dernière référence :
;; Décret n° 2018-1173 du 19 décembre 2018 portant relèvement du salaire minimum de croissance
(def smic 10.03)

;; Fixés au même endroit que le mode de calcul des bonifications.
;; Référence de calcul : Article D843-2 du CSS
(def bonification_taux_base 0.29101)
(def bonification_min_smic 59)
(def bonification_max_smic 120)
(def bonification_delta (/ bonification_taux_base (- bonification_max_smic bonification_min_smic)))

;; Référence de calcul : Article D843-2 du CSS
(defn calcul_taux_bonification [salaire]
	(if (> salaire (* bonification_max_smic smic)) bonification_taux_base (
		if (< salaire (* bonification_min_smic smic)) 0 (
			* bonification_delta (- (/ salaire smic) bonification_min_smic)
		)
	))
)

;; Le décret fixant ce montant est édité tout les ans, généralement en mai.
;; Dernière référence : 
;; Décret n° 2018-836 du 3 octobre 2018 portant revalorisation du montant forfaitaire de la prime d'activité et réduction de l'abattement appliqué aux revenus professionnels
(def montant_reference 551.51)

;; Référence de calcul : Article L843-3 du CSS
(defn calcul_montant_majore [personnes enfants isole salaire]
	(* montant_reference (+
		(calcul_taux_foyer personnes enfants isole)
		(calcul_taux_bonification salaire)
	))
)
```

# Calcul des ressources prises en compte

Nous mentionnions que la prime d’activité était une différence ; nous avons enfin terminé de calculer le premier terme, mais il nous reste le second ; rassurez-vous, il est bien plus simple. En effet, il vous suffit de consulter pour cela votre déclaration d’impôts : tous vos ressources soumises à l’impôt *sur le revenu* comptent dans les ressources de votre foyer ; en particulier, sont comptées[^15] :

- les revenus professionnels directs – en net – tirés d’une activité **salariée ou non** ;
- les revenus professionnels de remplacement, tels les revenus tirés de la mise à profit du congé individuel de formation, la rémunération perçue lors d’arrêts maladie, d’accident de travail ou de maladie professionnelle et les indemnités perçues à l’occasion des congés légaux de maternité, de paternité ou d’adoption[^16] ;
- l’aide légale ou conventionnelle aux salariés en chômage partiel ;
- les rémunérations perçues dans le cadre d’un contrat de volontariat dans la défense ou dans le cadre d’une action ayant pour objet l’adaptation à la vie active.

C’est tout ? Non bien sûr : quelques exceptions sont de rigueur.

## Les prestations et aides sociales

Nous voilà dans la partie la plus compliquée ; avec des dizaines de prestations sociales, difficile de connaître celles entrant en compte dans le calcul de la prime d’activité… Deux aides sont bien connues pour entrer de manière obligatoire dans son calcul, et nous allons donc les détailler ci-dessous. Pour le reste des aides, partez du principe qu’elles sont comptées, et si vous souhaitez plus de détails, la liste des exceptions est trouvable à l’[article R. 844-5 du CSS](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000038787584&cidTexte=LEGITEXT000006073189).

### Les aides au logement

Très simple : les aides au logement sont comptées **en intégralité** dans les ressources du foyer, pour tous les membres du foyer, sauf dans un cas rare : si, dans le foyer, une personne touche des **A**ides **P**ersonnalisées au **L**ogement, tandis que le reste du foyer bénéficie de la mise à disposition d’un logement à titre gratuit – voir ci-dessous. Dans ce cas, les APL du membre ne comptent pas dans le calcul de la prime d’activité si ces aides sont calculées sur les ressources propres de cette personne[^17].

### Les allocations destinées aux familles

Les allocations destinées aux familles sont comptées de manière plus complexes que les aides au logement ; pour faire simple, ces allocations sont divisées en trois morceaux :

- l’allocation de base[^18], touchée dès le premier enfant *de moins de 3 ans* et disposant de deux tranches en fonctions des ressources ; nous ne détaillerons pas cette allocation, car, en raison de sa finalité sociale particulière, **elle ne compte pas** dans les ressources du foyer pour la prime d’activité, vous pouvez donc la toucher sans rien déduire ;
- les allocations familiales[^19], versées dès le deuxième enfant de moins de 20 ans à charge, quelles que soient vos ressources personnelles, mais avec trois tranches différentes ; de même, elle ne sont pas prises en compte pour la prime d’activité ;
- le complément familial[^20], versé dès trois enfants de moins de 21 ans si le foyer ne dépasse pas un plafond de revenus ; ce complément est compté à hauteur de 41,65 % dans les ressources du foyer pour le calcul de la prime d’activité.

Je vous propose un petit tableau pour résumer tout ça (attention, il est très synthétique, et les montants sont approximatifs, se référer à la CAF pour le détail) :

| Nombre d’enfants |   Ressources du foyer   | Aides prises en compte |
| ---------------- | ----------------------- | ---------------------- |
|        1         |       Indifférent       |          Non           |
|        2         |       Indifférent       |          Non           |
|        3         | Moins de 45 000 € / an  |  Complément familial   |
|        3         |  Plus de 45 000 € / an  |          Non           |
|        4         | Moins de 55 000 € / an  |  Complément familial   |
|        4         |  Plus de 55 000 € / an  |          Non           |

### L’allocation de soutien familial

L’allocation de soutien familial[^21], versée pour les enfants n’ayant qu’un unique ou aucun parent aux suites d’un divorce, de sa non-reconnaissance à la naissance, d’un décès parental, ou d’un abandon, est comptée dans la prime d’activité à hauteur de[^22] :

- 30 % de l’allocation de base pour les enfants orphelins ;
- 22,5 % de l’allocation de base pour les enfants dont la filiation n’est pas légalement établie ;
- pas du tout pour les autres cas.

## Le forfait logement

Pour terminer le calcul des ressources du foyer, si vous disposez d’un logement à titre gratuit – ce qui inclus les propriétaires, qui ne payent donc plus leur logement, il faudra le compter de manière forfaitaire[^23] :

- pour une personne seule, 12 % du montant forfaitaire de base $M_{f_0}$, soit 66,18 € ;
- pour deux personnes, 16 % du montant forfaitaire $M_f$ calculé pour deux personnes (pour rappel, il est alors multiplié par un coefficient), soit 132,36 € ;
- au-delà, le forfait est plafonné à 16,5 % du montant forfaitaire $M_f$ calculé pour trois personnes, soit 163,8 €.

## Notre exemple : un logement

Pour reprendre notre exemple, la famille touche 28 800 € par an, ce qui est bien en-dessous du plafond approximatif du complément familial (45 000 €) ; cette famille touche donc :

- une allocation de base et les allocations familiales, qui n’entrent pas dans le calcul ;
- le complément familial $C_F$, à hauteur de 171,23 € par mois, comme en témoigne leur récépissé CAF ;
- nous admettrons aussi qu’ils bénéficient d’un logement à titre gratuit, évalué forfaitairement à 163,8 € dans leur cas (voir explications ci-dessus).

Leurs ressources sont alors égales à :

$$
\begin{matrix}
R_f	&=& S_{i_1} + S_{i_2} + C_F + 163,8 \\
	&=& 1 300 + 1 100 + 171,23 + 163,8 \\
R_f	&=& 2 735 €
\end{matrix}
$$

On calcule donc le montant de la prime d’activité en effectuant la différence entre le montant forfaitaire majoré $M_f'$ et les ressources du foyer $R_f$ :

$$
\begin{matrix}
M_{PA}	&=& M_f' - R_f \\
		&=& 3 136 - 2 735 \\
M_{PA}	&=& 401 €
\end{matrix}
$$

Nous y voilà arrivés ! Comme vous pouvez le voir, le calcul n’est pas si simple, mais ne contient pas d’éléments particulièrement complexes. Cette prime d’activité n’est pas des plus simple, mais elle traite tout les concepts de calcul des aides sociales ; c’est pourquoi j’ai décidé d’en parler en premier.

# Fin du simulateur

La partie de calcul des ressources du foyer de mon simulateur en Clojure est proposée ci-dessous :

```clj
;; Ce montant n'est pas changé de façon régulière, mais fait parfois l'objet d'ajustements.
;; Référence de calcul : Article D843-3 du CSS
(def taux_ressources_forfait 0.61)

;; Référence de calcul : Article D843-3 du CSS
(defn calcul_taux_ressources_forfait [ressources]
	(* taux_ressources_forfait ressources)
)

;; Le montant ici est entendu en multiple du montant forfaitaire.
;; Référence de calcul : Article R844-3 du CSS
(def taux_logement_un 0.12)
(def taux_logement_deux 0.16)
(def taux_logement_base 0.165)

;; Précalcul des forfaits logement possibles
(def montant_forfaitaire_un (* (mf/calcul_taux_foyer 1 0 false) mf/montant_reference))
(def montant_forfaitaire_deux (* (mf/calcul_taux_foyer 2 0 false) mf/montant_reference))
(def montant_forfaitaire_base (* (mf/calcul_taux_foyer 3 0 false) mf/montant_reference))

;; Référence de calcul : Article R844-3 du CSS
(defn calcul_avantage_logement [personnes]
	(if (< personnes 3) (
		if (< personnes 2) (* taux_logement_un montant_forfaitaire_un) (* taux_logement_deux montant_forfaitaire_deux)
	) (* taux_logement_base montant_forfaitaire_base))
)

;; Référence de calcul : Article R844-4 du CSS
(def taux_base_cfm 0.4165)
(def taux_base_asm_orphelin 0.3)
(def taux_base_asm_filiation 0.225)

;; Les avantages dus aux allocations familiales sont traités spécifiquement.
;; Référence de calcul : Article R844-4 du CSS
;; Format d'argument d'entrée :
;; { :base 5000 :majoration 200 :complement 0 :soutien 0 :orphelin false :filiation true }
(defn calcul_avantage_allocations [params]
	(def soutien (params :soutien 0))

	(+
		(params :base 0)
		(params :majoration 0)
		(* taux_base_cfm (params :complement 0))
		(if (params :orphelin false) (* taux_base_asm_orphelin soutien) (
			if (params :filiation false) (* taux_base_asm_filiation soutien) soutien
		))
	)
)
```

[^1]: Un foyer est un ensemble de personne partageant une même **déclaration fiscale** ; un foyer peut être constitué d’un seul membre.
[^2]: Un age est dit « révolu » passé le jour suivant l’anniversaire ; par exemple, une personne née le 20 avril 2003 aura 18 ans révolus au 21 avril 2021.
[^3]: Voir article [L. 842-2 du CSS](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000031087615&cidTexte=LEGITEXT000006073189), en particulier le 2°.
[^4]: Voir [L. 124-1 du code de l’éducation](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071191&idArticle=LEGIARTI000029233449), et [L. 6211-1 du code du travail](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006903991) pour le détail des personnes considérées étudiantes en droit.
[^5]: La personne ne fait alors plus partie du même *foyer*, c’est pourquoi elle peut percevoir la prime. C’est un peu plus compliqué que cela en réalité, voir [L. 842-2 du CSS](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000031087615&cidTexte=LEGITEXT000006073189), en particulier le 3°.
[^6]: Voir article [L. 1261-3](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072050&idArticle=LEGIARTI000006901377) du code du travail pour comprendre cette qualification.
[^7]: Calcul mentionné au [L. 842-3 du CSS](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000037994814&cidTexte=LEGITEXT000006073189).
[^8]: Voir [D. 843-1 du CSS](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000031673885&cidTexte=LEGITEXT000006073189) pour les taux mis à jour.
[^9]: Est considérée comme isolée, au sens de l’[article L. 842-7 du CSS](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000031077047&cidTexte=LEGITEXT000006073189), toute personne ne partageant pas ses ressources au sein d’un foyer **et** ayant à charge un ou plusieurs enfants ou ayant effectué une déclaration de grossesse.
[^10]: Article [D. 843-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000037873473&cidTexte=LEGITEXT000006073189) pour une explication du principe de fonctionnement de la bonification individuelle.
[^11]: Voir le [Décret n° 2018-1197 du 21 décembre 2018](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000037846000) de « revalorisation exceptionnelle ».
[^12]: Cette formule est dérivée des textes règlementaires, et faite pour permettre une estimation **simple**.
[^13]: Cette fraction des revenus professionnels est mentionnée au 1° de l’article [L. 842-3 du CSS](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000037994814&cidTexte=LEGITEXT000006073189).
[^14]: La fraction en elle-même est fixée par décret simple et peut être trouvée à l’article [D. 843-3](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000037462909&cidTexte=LEGITEXT000006073189).
[^15]: Voir article [R. 844-1 du CSS](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000031675756&cidTexte=LEGITEXT000006073189) pour référence.
[^16]: Ces « revenus de remplacement » font l’objet d’une liste plus large en réalité, pouvant être trouvée à l’article [R. 844-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033979176&cidTexte=LEGITEXT000006073189).
[^17]: L’explication exacte – mais plus obscure – est donnée à l’[article R. 844-4](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000031676000&cidTexte=LEGITEXT000006073189).
[^18]: Définie à l’article [L. 531-1](https://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000029336644&idSectionTA=LEGISCTA000006156167&cidTexte=LEGITEXT000006073189) et suivants
[^19]: Les allocations familiales sont définies à l’article [L. 521-1 et suivants](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006156163&cidTexte=LEGITEXT000006073189) du CSS.
[^20]: [Article L. 522-1](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006156164&cidTexte=LEGITEXT000006073189) et suivants.
[^21]: Se référer à l’article [L. 523-1 et suivants](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006156165&cidTexte=LEGITEXT000006073189) du CSS.
[^22]: Voir les détails à l’[article R. 844-4](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000031676000&cidTexte=LEGITEXT000006073189).
[^23]: Voir [article R. 844-3](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000031675988&cidTexte=LEGITEXT000006073189).
