---
title: Mise en oeuvre de la PocketBeagle
author: Stalone
date: 2019-10-06 00:00:00
categories:
- [electronique]
- [internet]
---

Ce guide a pour objet de guider la configuration préliminaire d’une PocketBeagle. Cette petite carte, pratique et peu chère, est néanmoins un calvaire à faire fonctionner pour qui n’a jamais bidouillé un minimum avec les systèmes GNU/Linux et la configuration réseau. En particulier, la mise en place du *forwarding* demande une certaine compréhension de la configuration des interfaces.

<+++>

Le public visé ici va du débutant à l’utilisateur intermédiaire souhaitant suivre une procédure simple plutôt que de se prendre la tête dans la configuration à chaque fois. Il se base sur un système hôte GNU/Linux existant, bien qu’il devrait être possible de l’adapter sans grandes difficultés pour les systèmes Mac OS. L’installation du système hôte n’est pas expliquée dans ce tutoriel, une VirtualBox ou équivalent devra donc avoir été mise en place au préalable.

# Préparation de la carte SD

## Bien choisir une carte

Les systèmes embarqués de cette sorte, fonctionnant sur carte SD, sont sensible à la vitesse de lecture/écriture d’icelles ; plus ces cartes sont de bonne qualité, plus le système *bootera* rapidement. Voici un petit tableau récapitulatif des modèles de carte :

| Vitesse d’écriture |  Classe  | Classe haute vitesse | Recommandée? |
| ------------------ | -------- | -------------------- | ------------ |
|       2 Mo/s       | Classe 2 |          ❌          |      🚫      |
|       4 Mo/s       | Classe 4 |          ❌          |      🚫      |
|       6 Mo/s       | Classe 6 |          ❌          |      🚫      |
|       10 Mo/s      |Classe 10 |       Classe 1       |      ✔️      |
|       30 Mo/s      |    ❌    |       Classe 3       |      ✅      |
|     >= 60 Mo/s     |    ❌    |          ❌          |      ✅      |

Une carte inférieure à la classe 10 standard résultera en de très mauvaises performances globales pour votre PocketBeagle, je **ne recommande pas du tout** de les utiliser. La classe 10 est un très bon rapport qualité-prix, puisqu’elle coûte environ le même prix pour des performances bien meilleures.

## Installation du système Debian fourni

Les BeagleBone sont réputées pour être très ouvertes, et pour posséder un écosystème très vaste ; toutefois, la PocketBeagle, en raison de sa complexité de mise en oeuvre pour les mainteneurs de systèmes, ne dispose globalement que d’un seul système d’exploitation : le Debian par défaut fourni par la fondation. Le système est fonctionnel mais, comme on dit, « c’est du Debian », il ne faut donc pas s’attendre à y trouver des paquets à jour à moins de passer en *testing* et de péter son système tous les quinze jours - c’est assez peu recommandable pour de l’embarqué.

L’image disque peut être téléchargée [ici pour sa version sans interface graphique](https://beagleboard.org/latest-images), ce donc nous n’aurons pas besoin en l’absence de connectique vidéo sur la Pocket par défaut. Une fois téléchargée, un petit `unxz bone-debian-M.m-iot-armhf-yyyy-mm-dd-4gb.img.xz` permettra de décompresser l’image dans le même dossier.

La procédure proposée fait usage d’un logiciel nommé Etcher, mais basé sur Electron et donc assez lourd ; le logiciel intégré à votre distribution, `dd`, fera largement l’affaire ici, pour un impact moindre en mémoire vive et en utilisation de processeur, surtout pour graver une image par an. Avant toute chose, il s’agira de bien identifier le nom du périphérique sur votre système hôte ; pour cela, je vous conseille de débrancher tout les périphériques de stockage externes, à l’exception de votre carte SD, puis de lancer la commande `sudo fdisk -l` pour lister les disques. Un exemple de sortie de cette commande est :

```
Disque /dev/sda : 465,78 GiB, 500107862016 octets, 976773168 secteurs
Modèle de disque : ST500LM012 HN-M5
Unités : secteur de 1 × 512 = 512 octets
Taille de secteur (logique / physique) : 512 octets / 4096 octets
taille d’E/S (minimale / optimale) : 4096 octets / 4096 octets
Type d’étiquette de disque : dos
Identifiant de disque : 0x9df64e97

Périphérique Amorçage     Début       Fin  Secteurs Taille Id Type
/dev/sda1    *             2048 188745727 188743680    90G 83 Linux
/dev/sda2             188745728 207030271  18284544     8G 82 partition d’échange Linux / Solaris
/dev/sda3             207030272 499998719 292968448 367,7G 83 Linux

Disque /dev/sdb : 14,94 GiB, 16012804096 octets, 31275008 secteurs
Modèle de disque : microSD RDR     
Unités : secteur de 1 × 512 = 512 octets
Taille de secteur (logique / physique) : 512 octets / 512 octets
taille d’E/S (minimale / optimale) : 512 octets / 512 octets
```

Un rapide coup d’oeil nous permet d’identifier deux disques physiques : mon disque dur `/dev/sda` ainsi que le carte SD `/dev/sdb`, qui est ici branchée via un lecteur de carte SD USB. Si votre ordinateur dispose d’un lecteur de carte SD intégré, le nom de votre carte ressemblera plutôt à `/dev/mmcblk0` ; bien entendu, **il ne faut pas se tromper de disque**, et utiliser ici le second, sans quoi vous casserez votre système. Mon premier disque contient trois partitions, et mon second aucune ; notons que ce n’est pas toujours le cas ; pour cette raison, nous allons d’abord écraser le contenu de la carte avant de la *flasher*, pour éviter les mauvaises surprises :

```bash
# ATTENTION au nom du disque, bien vérifier fdisk
# Remplacement des données antérieures de la carte par des données aléatoires
sudo dd if=/dev/urandom of=/dev/sdb bs=4M status=progress
# Ecriture de notre image disque
sudo dd if=bone-debian-M.m-iot-armhf-yyyy-mm-dd-4gb.img of=/dev/sdb bs=4M status=progress
```

# Premier *boot* et partage d’Internet

Maintenant que notre carte est prête, il suffit de l’insérer dans votre PocketBeagle, puis de la connecter à votre ordinateur via le câble USB fourni. Des LEDs vont clignoter de partout, preuve qu’il se passe un tas de choses. Après une longue minute, vous pourrez lancer la commande `sudo ifconfig` *sur votre hôte* pour afficher la liste des interfaces de votre machine ; vous devez y repérer l’interface via laquelle vous êtes connecté à Internet, qui commence par `w` en WiFi, et n’importe quelle autre lettre en Ethernet ; attention à `lo` qui est le loopback, et qu’il ne faudra pas toucher.

```
enp0s20u2: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.7.1  netmask 255.255.255.252  broadcast 192.168.7.3
        inet6 fe80::eef1:23f2:8883:ef1e  prefixlen 64  scopeid 0x20<link>
        ether 60:64:05:xx:xx:xx  txqueuelen 1000  (Ethernet)
        RX packets 326  bytes 44094 (43.0 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 294  bytes 53550 (52.2 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

enp0s20u2i2: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.6.1  netmask 255.255.255.252  broadcast 192.168.6.3
        inet6 fe80::a97f:7395:cc41:90b5  prefixlen 64  scopeid 0x20<link>
        ether 60:64:05:xx:xx:xx  txqueuelen 1000  (Ethernet)
        RX packets 230604  bytes 14332032 (13.6 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 409296  bytes 597610436 (569.9 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

enp3s0f1: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        ether xx:xx:xx:xx:xx:xx  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 111  bytes 11476 (11.2 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 111  bytes 11476 (11.2 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

wlp2s0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.2.192  netmask 255.255.255.0  broadcast 192.168.2.255
        inet6 fe80::aed5:7a63:ee58:d5e8  prefixlen 64  scopeid 0x20<link>
        ether xx:xx:xx:xx:xx:xx  txqueuelen 1000  (Ethernet)
        RX packets 462810  bytes 658466722 (627.9 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 278200  bytes 30582799 (29.1 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

Ceci dessus se trouve ma sortie `ifconfig` ; c’est très dense, mais nous n’avons pas besoin de toutes ces informations. Je suis ici connecté via WiFi, et repère donc que mon interface **de sortie** est `wlp2s0`, puis la PocketBeagle dispose de deux interfaces `enp0s20u2` (pour Windows) et `enp0s20u2i2` (pour Linux et Mac). Mon interface d’entrée Beagle - du point de vue de mon ordinateur - est donc `enp0s20u2i2`.

Afin de partager notre connexion Internet via l’interface exposée par la Beagle, il faut d’abord lancer des commandes autorisant ce partage sur la machine hôte :

```bash
$ sysctl net.ipv4.ip_forward=1
$ iptables --table nat --append POSTROUTING --out-interface enp0s20u2i2 -j MASQUERADE
$ iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
$ iptables -A FORWARD -i enp0s20u2i2 -o wlp2s0 -j ACCEPT
```

Sur la carte PocketBeagle, il faut indiquer le point de sortie vers le réseau : `route add default gw 192.168.6.1`. Une fois cette configuration effectuée, vous pouvez vérifier son fonctionnement en tentant un ping : `ping boxph.one`.

Comme vous le voyez, pas facile pour un débutant de s’y retrouver dans toutes ces commandes… Je dois moi-même me référer au guide à chaque fois que j’ai besoin d’activer le partage d’Internet.
