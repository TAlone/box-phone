---
title: 'Passer sa licence radioamateur : épreuve réglementaire'
author: Stalone
date: 2019-08-07 00:00:00
categories: electronique
---

Intéressé depuis longtemps par l’électronique analogique, il était évident que je finirais un jour par passer mon examen de radioamateur, ce afin d’obtenir une autorisation d’émettre. Le radioamateurisme est un domaine très intéressant et je vous conseille de vous y lancer si vous vous interessez aux techniques analogiques ; je vais mentionner dans cet article la manière dont j’ai commencé il y a peu, pour un coût très bas. Aussi, je détaillerais, au fur et à mesure de mes apprentissages, les bases nécessaires pour passer l’examen règlementaire ; la partie électronique, que je connais déjà en grande partie, fera l’objet d’un article ultérieur.

<+++>

# Comment débuter

## Le matériel

Je m’intéresse depuis quelques temps à la radio, mais n’ai jamais voulu me lancer dans le radioamateurisme car j’imaginais qu’il faillait nécessairement acheter du matériel couteux et encombrant. **Je m’étais trompé**, et un radioamateur croisé au détour d’un festival me l’a très vite fait savoir : lui-même avait commencé avec un petit appareil à quelques dizaine d’euros, du nom de Baofeng.

Les Baofeng sont de petites radios portables, pouvant recevoir sur les bandes VHF et UHF et émettre avec une puissance variable. En France, ces appareils sont en vente libre sur tous les sites de e-commerce ; le principal importateur est [Radioddity](https://www.radioddity.com/), qui propose un grand lot de modèles. J’ai choisi le modèle UV-5R pour son prix modique – 25 € environ, et sa puissance d’émission correcte – 5 Watts pleine puissance.

L’appareil m’a été livré avec une station de recharge et son adaptateur secteur européen, la batterie et un set écouteur + microphone. L’écouteur fourni ne me semble pas particulièrement pratique, et la connectique Jack est à l’opposé de ce qu’on a l’habitude de voir – Jack 2.5 pour le casque, et 3.5 pour le micro, ce qui est le seul point noir de l’appareil : il vous faudra trouver un adaptateur Jack pour y brancher votre casque préféré.

## Un début : la radioécoute

Je suis très content de ce modèle qui fonctionne parfaitement, et après avoir acheté un adaptateur Jack, l’aisance d’écoute est très bonne. Avant de passer ma licence, je souhaitais écouter quelques conversations pour voir comment elles se passaient en pratique. Pour cela, trois moyens existent, et peuvent être utilisés avec le Baofeng :

- l’écoute directe, qui implique que des radioamateurs se trouvent à proximité de vous, pour les recevoir de façon intelligible ;
- l’écoute via relais, très pratique si vous avez un relais par chez vous, je l’utilise très souvent ; une [carte des relais](https://radioamateur.org/relais/) peut être trouvée en ligne pour les français (et probablement dans les autres pays) ;
- l’écoute par satellite, plus difficile car il faut suivre la position du satellite et être présent lors de son passage, mais très interessante car vous y entendrez presque à tous les coups une conversation ; je vous recommance le site officiel de l’[Association Internationale des Radioamateurs par Satellite](https://www.amsat.org/) pour trouver les satellites actifs, et le site [Heavens Above](https://heavens-above.com/) pour leur localisation en temps réel.

Un billet plus complet sera peut-être publié par la suite pour détailler ces différents modes d’écoute, mais pour le moment, je vous invite à vous renseigner via votre moteur de recherche favori, c’est très simple une fois qu’on connaît les mot-clefs donnés ci-dessus.

# L’examen : partie réglementaire

Pour rappel, un examen de radioamateur doit être passé dans l’une des antennes de l’**A**gence **N**ationale des **FR**équences ; il porte sur deux parties distinctes :

- la partie règlementaire, qui vise à vérifier vos connaissances de la réglementation nationale et internationale ;
- la partie sur les techniques de l’électricité, testant des connaissances *de base* en électronique, afin de pouvoir éventuellement dépanner un émetteur défectueux.

Cet article se concentre sur la partie règlementaire, et est **en cours d’écriture** ; je n’ai pas encore passé mon examen, et je le rédige donc en même temps que j’apprends. L’objectif est d’être prêt d’ici septemble pour passer l’examen – sachant que je connais déjà 90 % de la partie électronique, c’est pourquoi je me focalise sur le règlementaire pour le moment. Cet article est une reprise brute du [programme de l’examen](https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000025886823&cidTexte=LEGITEXT000020278007), complété des précisions nécessaires.

## Règlementation internationale, européenne et nationale

### Définitions du règlement des radiocommunications de l’UIT

- service d’amateur : service de radiocommunication ayant pour objet l’instruction individuelle, l’intercommunication et les études techniques, effectué par des amateurs, c’est-à-dire par des personnes dûment autorisées, s’intéressant à la technique de la radioélectricité à titre uniquement personnel et sans intérêt pécuniaire ;
- service d’amateur par satellite : Service de radiocommunication faisant usage de stations spatiales situées sur des satellites de la Terre pour les mêmes fins que le service d’amateur ;
- station d’amateur : station du service d’amateur.

### Extrait du Règlement – Article 25

#### Section I – Service d’amateur

25.1	§1	Les radiocommunications entre stations d’amateur de pays différents sont autorisées, sauf si l’administration de l’un des pays intéressés a notifié son opposition. (CMR-03)

25.2	§2	1) Les transmissions entre stations d’amateur de pays différents doivent se limiter à des communications en rapport avec l’objet du service d’amateur, tel qu’il est défini au numéro 1.56, et à des remarques d’un caractère purement personnel. (CMR-03)

25.2A		1A) *Il est interdit de coder les transmissions entre des stations d’amateur de différents pays pour en obscurcir le sens, sauf s’il s’agit des signaux de commande échangés entre des stations terriennes de commande et des stations spatiales du service d’amateur par satellite. (CMR-03)*

25.3		2) Les stations d’amateur peuvent être utilisées pour transmettre des communications internationales en provenance ou à destination de tierces personnes seulement dans des situations d’urgence ou pour les secours en cas de catastrophe. Une administration peut déterminer l’applicabilité de cette disposition aux stations d’amateur relevant de sa juridiction. (CMR-03)

25.4		*Supprimé*

25.5	§3	1) Les administrations déterminent si une personne qui souhaite obtenir une licence pour exploiter une station d’amateur doit ou non démontrer qu’elle est apte à la transmission et à la réception de textes en signaux du code Morse. (CMR-03)

25.6		2) Les administrations vérifient les aptitudes opérationnelles et techniques de toute personne qui souhaite exploiter une station d’amateur. D’ Des lignes directrices relatives aux niveaux de compétence requis sont indiquées dans la version la plus récente de la Recommandation UIT-R M.1544. (CMR-03)

25.7	§4	La puissance maximale des stations d’amateur est fixée par les administrations concernées. (CMR-03)

25.8	§5	1)   Tous les Articles ou dispositions pertinents de la Constitution, de la Convention et du présent Règlement s’appliquent aux stations d’amateur. (CMR-03)

25.9		2)   Au cours de leurs émissions, les stations d’amateur doivent transmettre leur indicatif d’appel à de courts intervalles.

25.9A	§5A	Les administrations sont invitées à prendre les mesures nécessaires pour autoriser les stations d’amateur à se préparer en vue de répondre aux besoins de communication pour les opérations de secours en cas de catastrophe. (CMR-03)

25.9B	§5B	Une administration peut décider d’autoriser ou non une personne d’une autre administration qui a reçu une licence pour l’exploitation d’une station d’amateur à exploiter une station d’amateur, lorsque cette personne se trouve temporairement sur son territoire, sous réserve des conditions ou des restrictions qu’elle pourrait imposer. (CMR-03)

#### Section II – Service d’amateur par satellite

25.10	§6	Les dispositions de la Section I du présent Article s’appliquent, s’il y a lieu, de la même manière au service d’amateur par satellite.

25.11	§7	Les administrations autorisant des stations spatiales du service d’amateur par satellite doivent faire en sorte que des stations terriennes de commande en nombre suffisant soient installées avant le lancement, afin de garantir que tout brouillage préjudiciable causé par des émissions d’une station du service d’amateur par satellite puisse être éliminé immédiatement (voir le numéro 22.1). (CMR-03)

### Bandes de fréquences autorisées

Théoriquement, toutes les fréquences suivantes sont attribuées en France aux radioamateurs ; en pratique, seules celles ayant un nom sont couramment demandées à l’examen. Le nom est courant, pas officiel ; le feuillet réfère au TNRBF, qui est un document administratif répertoriant toutes les fréquences et leurs attributions.

|   Nom    |           Bande           | Feuillet  |         Commentaire         |
| -------- | ------------------------- | --------- | --------------------------- |
|          |    135,70 – 135,80 kHz    |    03a    |    PIRE limitée à 1 Watt    |
|          |    472,00 – 479,00 kHz    |    04a    |    PIRE limitée à 1 Watt    |
|   160m   |   1810,00 – 1850,00 kHz   |    06a    | différent selon les régions |
|   80m    |   3500,00 – 3800,00 kHz   |    10a    | différent selon les régions |
|   40m    |   7000,00 – 7200,00 kHz   |    14a    | différent selon les régions |
|   30m    | 10 100,00 – 10 150,00 kHz |    15a    |   interdite par satellite   |
| 20m (HF) | 14 000,00 – 14 350,00 kHz |    18a    |                             |
|   17m    | 18 068,00 – 18 168,00 kHz |    19a    |                             |
|   15m    | 21 000,00 – 21 450,00 kHz |    20a    |                             |
|   12m    | 24 890,00 – 24 990,00 kHz |    22a    |                             |
|          |    28,000 – 29,700 MHz    |    23a    |                             |
|          |    50,000 – 52,000 MHz    |    25a    |   interdite par satellite   |
| 2m (VHF) |   144,000 – 146,000 MHz   |    29a    | différent selon les régions |
|23cm (UHF)|   430,000 – 440,000 MHz   | 37 et 38a | différent selon les régions |
|          | 1 240,000 – 1 300,000 MHz |    43a    |                             |
|          | 2 300,000 – 2 450,000 MHz |    54a    |                             |
|          | 5 650,000 – 5 850,000 MHz |    60a    |                             |
|          |     10,00 – 10,50 GHz     |    65a    |                             |
|          |     24,00 – 24,25 GHz     |    76a    |                             |
|          |     47,00 – 47,20 GHz     |    83a    |                             |
|          |     76,00 – 81,00 GHz     |    86a    |                             |
|          |    122,25 – 123,00 GHz    |    88a    |                             |
|          |    134,00 – 141,00 GHz    |    89a    |                             |
|          |    241,00 – 250,00 GHz    |    91a    |                             |

### Puissances d’émission

Depuis la délivrance d’un certificat unique d’opérateur, la PIRE est limitée en fonction de la fréquence uniquement. **En dehors des exceptions** mentionnées dans le tableau ci-dessus, les puissances maximales sont les suivantes :

- 500W en-dessous de 25 MHz ;
- 250W entre 28 MHz et 29,7 MHz
- 120W au-dessus de 50 MHz.

### Régions radioélectriques de l’UIT

![Illustration des différentes régions radioélectriques de l’UIT : région 1 Europe / Asie / Afrique ; région 2 Asie du Sud / Océanie ; région 3 Amérique](Zones_Radio.png)

### Indicatifs d’appel

L’indicatif d’appel va comme suit : PPXSSSS, avec :

- PP, préfixe identifiant le pays du radioamateur ; pour la France **métropolitaine** il s’agit de la lettre F seule ; 
- X, chiffre donnant la classe de la licence du radioamateur, maintenant, seul le chiffre 4 est utilisé pour les nouveaux indicatifs ; 
- SSSS, suffixe attribué à la station de deux à quatre lettres : AAA à UZZZ et AA à ZZ désignent des indicatifs personnels, KAA à KZZ pour les radio-clubs (KA à KZ pour les radio-clubs des DROM-COM et de Corse), VAA à VZZ pour les radioamateurs de l’UE pour plus de 3 mois en France, WAA à WZZ pour les radioamateurs étrangers à l’UE pour plus de 3 mois en France, XAA à XZZ et YAA à YZZ sont en réserve, enfin, ZAA à ZZZ sont réservés aux stations répétitrices et aux balises. 
- ce groupe de caractères peut est complété par : 
    - /P pour les activités portables (émetteur ne pouvant pas être utilisé en déplacement), 
    - /M pour les activités mobiles (émetteur pouvant être utilisé en déplacement), 
    - /MM pour les activités maritimes mobiles

Cette partie est une libre adaptation de la page [Procédures Radio de Wikibooks](https://fr.wikibooks.org/wiki/Pr%C3%A9paration_au_certificat_d%27op%C3%A9rateur_du_service_amateur/Proc%C3%A9dures_radio).

### Préfixes européens

Voir [ce site](http://www.on4sh.be/ham/prefix.htm) pour la liste complète de préfixes ; pour l’examen, seuls les préfixes européens (CEPT), rappellés ci-dessous, sont demandés :

|        Pays        | Indicatif |        Pays        | Indicatif |        Pays        | Indicatif |
| ------------------ | --------- | ------------------ | --------- | ------------------ | --------- |
|      Albanie       |    ZA     |      Hongrie       |  HA, HG   |      Pays-Bas      |    PA     |
|     Allemagne      |    DL     |      Islande       |    TF     |      Pologne       |    SP     |
|      Autriche      |    OE     |      Irlande       |  EI, EJ   |      Roumanie      |    YO     |
|    Biélorussie     |    EW     |       Italie       |     I     |       Russie       |    RA     |
|      Belgique      |    ON     |      Lettonie      |    YL     |       Serbie       |    YU     |
| Bosnie-Herzégovine |    E7     |   Liechtenstein    |    HB0    |     Slovaquie      |    OM     |
|      Bulgarie      |    LZ     |     Lithuanie      |    LY     |      Slovénie      |    S5     |
|      Croatie       |    9A     |     Luxembourg     |    LX     |       Suède        |  SM, SA   |
|       Chypre       |    5B     |     Macédoine      |    Z3     |       Suisse       |    HB9    |
|      Espagne       |    EA     |      Moldavie      |    ER     |      Tchéquie      |    OK     |
|      Estonie       |    ES     |       Monaco       |    3A     |      Turquie       |    TA     |
|       Grèce        |    SV     |     Montenegro     |    4O     |      Ukraine       |    UT     |

Certains pays disposant de morceaux de terre détachés (îles ou non), bénéficient de multiples préfixes dérivés :

|        Pays        | Indicatif |       Dérivé       | Indicatif |       Dérivé       | Indicatif |
| ------------------ | --------- | ------------------ | --------- | ------------------ | --------- |
|      Danemark      |    OZ     |    - Îles Faroe    |    OY     |    - Groenland     |    OX     |
|      Portugal      |    CT7    |      - Azores      |    CT8    |      - Madère      |    CT9    |
|      Finlande      |    OH     |    - Îles Aland    |    OH0    |
|      Norvège       |    LA     |     - Svalbard     |    JW     |

Pour la France, le préfixe est F en métropole et varie pour les départements ultra-marins :

- Corse : TK
- Guadeloupe : FG
- Guyane : FY
- Martinique : FM
- Saint-Barthélémy : FJ
- Saint-Pierre-et-Miquelon : FP
- Saint-Martin : FS
- Réunion : FR
- Mayotte : FH
- Antarctique : FT
- Polynésie : FO
- Nouvelle-Calédonie : FK
- Wallis & Futuna : FW

Pour le Royaume-Uni, le préfixe standard est M et les variantes vont comme suit :

- Île de Man : MD
- Irelande du Nord : MI
- Jersey : MJ
- Ecosse : MM
- Guernesay : MU
- Pays de Galle : MW

### Communication de catastrophe

Je propose ci-dessous une transcription textuelle du scan de la résolution n° 640 fourni par l’ITU ; toutes les choses importantes sont contenues dans ce document datant de la conférence de Genève de 1979.

RÉSOLUTION N° 640 relative à l’utilisation internationale, en cas de catastrophe naturelle, des radiocommunications dans les bandes de fréquences attribuées au service d’amateur

La Conférence administrative mondiale des radiocommunications (Genève, 1979), considérant :

- a) qu’en cas de catastrophe naturelle, les systèmes de communication normaux sont fréquemment surchargés, endommagés ou totalement inutilisables ;
- b) qu’il est indispensable de rétablir rapidement les communications pour faciliter les opérations de secours organisées à l’échelle mondiale ;
- c) que les bandes attribuées au service d’amateur ne sont pas soumises à des plans internationaux ou à des procédures de notification et qu’elles se prêtent donc bien à une utilisation à court terme dans les cas d’urgence ;
- d) que les communications internationales en cas de catastrophe seraient facilitées par le recours provisoire à certaines bandes de fréquences attribuées au service d’amateur ;
- e) que, dans de telles circonstances, les stations du service d’amateur, en raison de leur large dispersion et de leur capacité démontrée dans des cas semblables, peuvent aider à répondre aux besoins essentiels en communica-tions ;
- f) qu’il existe des réseaux nationaux et régionaux d’amateur, pour les cas d’urgence, qui utilisent certaines fréquences dans les bandes attribuées au service d’amateur ;
- g) qu’en cas de catastrophe naturelle, la communication directe entre les stations du service d’amateur et d’autres stations pourrait se révéler utile, notamment pour effectuer des communications indispensables jusqu’au rétablissement des communications normales ;

reconnaissant que les droits et les responsabilités en matière de communications en cas de catastrophe naturelle relèvent des administrations concernées ; décide

1. que les bandes attribuées au service d’amateur, spécifiées au numéro 510, peuvent être utilisées par les administrations pour répondre aux besoins de communications internationales en cas de catastrophe ;
2. que ces bandes ainsi utilisées ne doivent servir qu’à des communications se rapportant à des opérations de secours en cas de catastrophe naturelle ;
3. que, pour les communications en cas de catastrophe, l’utilisation des bandes attribuées au service d’amateur par des stations n’appartenant pas à ce service doit être limitée à la période d’urgence et aux zones géographiques particulières, définies par l’autorité responsable du pays affecté ;
4. que les communications établies en cas de catastrophe doivent être effectuées à l’intérieur de la zone sinistrée et entre la zone sinistrée et le siège permanent de l’organisation assurant les opérations de secours ;
5. que de telles communications ne doivent être effectuées qu’avec le consentement de l’administration du pays frappé par la catastrophe ;
6. que les communications de secours d’origine extérieure au pays sinistré ne doivent pas remplacer les réseaux d’amateur nationaux ou internationaux déjà prévus pour les situations d’urgence ;
7. qu’une étroite collaboration est souhaitable entre les stations du service d’amateur et les stations d’autres services de radiocommunication qui pourraient estimer nécessaire d’utiliser les fréquences attribuées au service d’amateur pour les communications en cas de catastrophe ;
8. que de telles communications internationales de secours doivent, dans la mesure du possible, éviter de causer des brouillages aux réseaux du service d’amateur ;

invite les administrations

1. à satisfaire aux besoins pour les communications internationales en cas de catastrophe ;
2. à prévoir, dans leur réglementation nationale, les moyens de satisfaire aux besoins pour les communications d’urgence.

En pratique, il ne faut pas refuser de relayer une urgence, et aucun dédommagement ne sera fourni lors de ces communications de catastrophe. Les signaux de détresse peuvent être émis avec un « SOS » (en Morse : ··· ––– ···) lors des cas d’urgences extremes.

## Conventions et codes radio

### Alphabet international

| L.|   AI    | L.|    AI    | L.|   AI   |
| - | ------- | - | -------- | - | ------ |
| A |  alfa   | M |   mike   | Y | yankee |
| B |  bravo  | N | november | Z |  zulu  |
| C | charlie | O |  oscar   | 0 |  zero  |
| D |  delta  | P |   papa   | 1 |  one   |
| E |  echo   | Q |  quebec  | 2 |  two   |
| F | foxtrot | R |  romeo   | 3 | three  |
| G |  golf   | S | sierra   | 4 |  four  |
| H |  hotel  | T |  tango   | 5 |  five  |
| I |  india  | U | uniform  | 6 |  six   |
| J | juliett | V |  victor  | 7 | seven  |
| K |  kilo   | W | whiskey  | 8 | eight  |
| L |  lima   | X |  x-ray   | 9 |  nine  |

### Code Q

Le code Q est un code universel mis en place pour communiquer rapidement ; de **très nombreux** codes [sont disponibles](https://en.wikipedia.org/wiki/Q_code), mais seule la partie demandée à l’examen est listée ici depuis l’annexe du Journal Officiel fixant les conditions d’obtention du certificat d’opérateur.

| Abréviation | Question posée | Réponse donnée |
| ----------- | -------------- | -------------- |
|     QRA     | Quel est le nom de votre station ? | Le nom de ma station est \[…\]. |
|     QRG     | Voulez-vous m’indiquer ma fréquence exacte (ou la fréquence exacte de …) ? | La fréquence demandée est d’exactement \[…\] (k/M/G)Hz. |
|     QRH     | Ma fréquence varie-t-elle ? | Votre fréquence varie. |
|     QRK     | Quelle est l’intelligibilité de mon signal (ou du signal de …) ? | L’intelligibilité du signal en question est \[…\]. => voir quantificateurs chiffrés |
|     QRL     | Êtes-vous occupé ? | Je suis occupé. Prière de ne pas brouiller. |
|     QRM     | Êtes-vous brouillé ? | \[…\] => voir quantificateurs chiffrés |
|     QRN     | Êtes-vous troublé par des parasites ? | \[…\] => voir quantificateurs chiffrés |
|     QRO     | Dois-je augmenter la puissance d’émission ? | Augmentez la puissance d’émission. |
|     QRP     | Dois-je diminuer la puissance d’émission ? | Diminuez la puissance d’émission. |
|     QRT     | Dois-je cesser la transmission ? | Cessez la transmission. |
|     QRU     | Avez-vous quelque chose pour moi ? | Je n’ai rien pour vous. |
|     QRV     | Êtes-vous prêt ? | Je suis prêt. |
|     QRX     | À quel moment me rappellerez-vous ? | Je vous rappellerai à \[…\] heures (sur \[…\] (k/M/G)Hz). |
|     QRZ     | Par qui suis-je appelé ? | Vous êtes appelé par \[…\] (sur \[…\] (k/M/G)Hz). |
|     QSA     | Quelle est la force de mes signaux (ou des signaux de …) ? | \[…\] => voir quantificateurs chiffrés |
|     QSB     | La force de mes signaux varie-t-elle ? | La force des signaux est \[…\]. => => voir quantificateurs chiffrés |
|     QSL     | Pouvez-vous me donner accusé de réception ? | Je vous donne accusé de réception. |
|     QSO     | Pouvez-vous communiquer avec … ? | Je peut communiquer avec iel directement (ou par l’intermédiaire de \[…\]). |
|     QSP     | Voulez-vous retransmettre à … gratuitement ? | Je peux retransmettre. |
|     QSY     | Dois-je passer à la transmission sur une autre fréquence ? | Passez à la transmission sur une autre fréquence (ou sur \[…\] (k/M/G)Hz). |
|     QTH     | Quelle est votre position en latitude et en longitude ? | Ma position est \[…\] latitude \[…\] longitude. |
|     QTR     | Quelle est l’heure exacte ? | L’heure exacte est \[…\]. |

Pour les questions nécessitant une réponse quantifiée, il faut utiliser une échelle de 1 à 5 pour fournir la réponse.

### Types de modulation

Les modulations sont décrites selon un code comprenant :

- une première lettre représentant le type de modulation utilisée ;
- un chiffre, indiquant le type de signal modulant ;
- une dernière lettre, pour indiquer le type de donnée transmise.

Un résumé est disponible dans le tableau suivant regroupant les combinaisons possibles pour l’examen :

|                              Modulation                               |                        Signal modulant                         |          Type de signal modulé          |
| --------------------------------------------------------------------- | -------------------------------------------------------------- | --------------------------------------- |
|  A : Modulation d’amplitude, double bande latérale, porteuse normale  | 1 : Signal numérique sans emploi d’une sous-porteuse modulante |   A : Télégraphie à réception auditive  |
|  R : Modulation d’amplitude, bande latérale unique, porteuse réduite  | 2 : Signal numérique avec emploi d’une sous-porteuse modulante | B : Télégraphie à réception automatique |
| J : Modulation d’amplitude, bande latérale unique, porteuse supprimée |                     3 : Signal analogique                      |              C : Fac-similé             |
|         C : Modulation d’amplitude, bande latérale résiduelle         |                 7 : Plusieurs voies numériques                 |               D : Données               |
|                      F : Modulation de fréquence                      |                8 : Plusieurs voies analogiques                 |              E : Téléphonie             |
|                        G : Modulation de phase                        |         9 : Plusieurs voies analogiques et numériques          |              F : Télévision             |
